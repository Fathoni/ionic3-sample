import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { InboxPage } from '../pages/inbox/inbox';
import { DetailInboxPage } from '../pages/detail-inbox/detail-inbox';
import { OutboxPage } from '../pages/outbox/outbox';
import { KontakPage } from '../pages/kontak/kontak';
import { PesanBaruPage } from '../pages/pesan-baru/pesan-baru';
import { DetailPesanPage } from '../pages/detail-pesan/detail-pesan';
import { TambahKontakPage } from '../pages/tambah-kontak/tambah-kontak';
import { SettingIpPage } from '../pages/setting-ip/setting-ip';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalProvider } from '../providers/global/global';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    InboxPage,
    OutboxPage,
    PesanBaruPage,
    DetailPesanPage,
    KontakPage,
    TambahKontakPage,
    SettingIpPage,
    DetailInboxPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    InboxPage,
    OutboxPage,
    PesanBaruPage,
    DetailPesanPage,
    KontakPage,
    TambahKontakPage,
    SettingIpPage,
    DetailInboxPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider
  ]
})
export class AppModule {}
