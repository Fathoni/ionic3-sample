import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PesanBaruPage } from '../pesan-baru/pesan-baru';
import { InboxPage } from '../inbox/inbox';
import { OutboxPage } from '../outbox/outbox';
import { KontakPage } from '../kontak/kontak';
import { SettingIpPage } from '../setting-ip/setting-ip';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  pesanBaru() {
    this.navCtrl.push(PesanBaruPage)
  }
  
  inbox() {
    this.navCtrl.push(InboxPage)
  }
  
  outbox() {
    this.navCtrl.push(OutboxPage)
  }
  
  kontak() {
    this.navCtrl.push(KontakPage)
  }

  settingIP() {
    this.navCtrl.push(SettingIpPage)
  }

}
