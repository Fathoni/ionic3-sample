import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingIpPage } from './setting-ip';

@NgModule({
  declarations: [
    SettingIpPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingIpPage),
  ],
})
export class SettingIpPageModule {}
