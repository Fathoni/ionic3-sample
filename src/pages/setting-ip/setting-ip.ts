import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

/**
 * Generated class for the SettingIpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting-ip',
  templateUrl: 'setting-ip.html',
})
export class SettingIpPage {

  ip_address = ''

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage
  ) {
    storage.get('ip_address').then((val) => {
      console.log(val)
      if (val != null) {
        this.ip_address = val
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingIpPage');
  }

  simpan() {
    this.storage.set('ip_address', this.ip_address);
    this.navCtrl.setRoot(HomePage)
  }

}
