import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { TambahKontakPage } from '../tambah-kontak/tambah-kontak';

/**
 * Generated class for the KontakPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kontak',
  templateUrl: 'kontak.html',
})
export class KontakPage {
  lists: any
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider,
    public events: Events
  ) {
    this.initEvent()
    this.events.publish('refreshContact')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KontakPage');
  }

  initEvent() {
    this.events.subscribe('refreshContact', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      let url = 'contact'
      this.global.get(url)
      .then(out => {
        console.log(out)
        let data = (out as any).data.data
        this.lists = data
        console.log(out);
      })
    });
  }

  addContact() {
    this.navCtrl.push(TambahKontakPage, {
      act: 'add',
      item: {}
    })
  }

  editContact(item) {
    this.navCtrl.push(TambahKontakPage, {
      item: item,
      act: 'edit'
    })
  }

}
