import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { DetailPesanPage } from '../detail-pesan/detail-pesan';

/**
 * Generated class for the OutboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-outbox',
  templateUrl: 'outbox.html',
})
export class OutboxPage {

  lists: any
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider,
    public events: Events
  ) {
    this.loadOutbox()
    this.events.subscribe('refreshOutbox', () => {
      this.loadOutbox()
    })
  }

  loadOutbox () {
    let url = 'outbox?$sort[id]=-1'
    this.global.get(url)
    .then(out => {
      let data = (out as any).data.data
      this.lists = data
      console.log(out);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OutboxPage');
  }

  detailMessage(item) {
    this.navCtrl.push(DetailPesanPage, {
      item: item
    })
  }

}
