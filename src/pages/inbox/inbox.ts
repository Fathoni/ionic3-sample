import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import _ from 'lodash'
import { DetailInboxPage } from '../detail-inbox/detail-inbox';

/**
 * Generated class for the InboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {

  lists: any
  isWait = true
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider
  ) {
    let urlcontact = 'contact'
    this.global.get(urlcontact)
    .then(out => {
      console.log(out)
      let contact = (out as any).data.data
      
      let url = 'all-inbox'
      this.global.post(url, {tes: 'halo'})
      .then(out => {
        let data = (out as any).data.datasms

        data.forEach(element => {
          var search = _.find(contact, {phone: element.nomor})
          if (search === undefined) {
            element.contactname = element.nomor
          } else {
            element.contactname = search.name  
          }
          console.log('hasil lodash = ', _.find(contact, {phone: element.nomor}))
        });
        this.lists = data
        console.log(out);
        this.isWait = false
      })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InboxPage');
  }

  detail(item) {
    this.navCtrl.push(DetailInboxPage, {
      item: item
    })
  }

}
