import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TambahKontakPage } from './tambah-kontak';

@NgModule({
  declarations: [
    TambahKontakPage,
  ],
  imports: [
    IonicPageModule.forChild(TambahKontakPage),
  ],
})
export class TambahKontakPageModule {}
