import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the TambahKontakPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tambah-kontak',
  templateUrl: 'tambah-kontak.html',
})
export class TambahKontakPage {

  input = {
    name: '',
    phone: '',
    act: ''
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider,
    public events: Events
  ) {
    this.input.act = navParams.data.act
    if (this.input.act == 'edit') {
      this.input.name = navParams.data.item.name
      this.input.phone = navParams.data.item.phone
    }
    console.log(navParams);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TambahKontakPage');
  }

  hapus () {
    let url = 'contact'
    this.global.delete(url, this.input.phone)
      .then(out => {
        let status = (out as any).statusText
        this.events.publish('refreshContact')
        this.navCtrl.pop()
        console.log(out)
      })
  }

  simpan() {
    console.log(this.input)
    let url = 'contact'

    if (this.input.act == 'add') {
      this.global.post(url, this.input)
      .then(out => {
        let status = (out as any).statusText
        if (status == 'Created') {
          this.events.publish('refreshContact')
          this.navCtrl.pop()
        }
        console.log(out)
      })
    } else if (this.input.act == 'edit') {
      let params = {
        id: this.input.phone,
        phone: this.input.phone,
        name: this.input.name
      }
      this.global.patch(url, params)
      .then(out => {
        let status = (out as any).statusText
        if (status == 'OK') {
          this.events.publish('refreshContact')
          this.navCtrl.pop()
        }
        console.log(out)
      })
    }
    
  }

}
