import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailInboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-inbox',
  templateUrl: 'detail-inbox.html',
})
export class DetailInboxPage {
  details: any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let tujuan = ''
    if (navParams.data.item["phone.name"] != null) {
      tujuan = navParams.data.item["phone.name"]
    } else {
      tujuan = navParams.data.item.DestinationNumber
    }

    this.details = {
      tujuan: navParams.data.item.contactname,
      deskripsi: navParams.data.item.isi
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailInboxPage');
  }

}
