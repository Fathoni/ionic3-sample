import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailInboxPage } from './detail-inbox';

@NgModule({
  declarations: [
    DetailInboxPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailInboxPage),
  ],
})
export class DetailInboxPageModule {}
