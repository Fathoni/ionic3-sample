import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { OutboxPage } from '../outbox/outbox';
import moment from 'moment'

/**
 * Generated class for the PesanBaruPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pesan-baru',
  templateUrl: 'pesan-baru.html',
})
export class PesanBaruPage {

  input = {
    DestinationNumber: [],
    TextDecoded: '',
    InsertIntoDB: moment().format('YYYY-MM-DD hh:mm:ss'),
    SendingDateTime: moment().format('YYYY-MM-DD hh:mm:ss'),
    SendingTimeOut: moment().format('YYYY-MM-DD hh:mm:ss'),
  }

  listContact: any
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider,
    private alert: AlertController
  ) {
    let url = 'contact'
    this.global.get(url)
    .then(out => {
      let data = (out as any).data.data
      this.listContact = data
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesanBaruPage');
  }

  simpan() {
    let url = 'outbox'
    let arrpost = []

    console.log(this.input)
    this.input.DestinationNumber.forEach(element => {
      let param = {
        DestinationNumber: element,
        TextDecoded: this.input.TextDecoded,
        InsertIntoDB: this.input.InsertIntoDB,
        SendingDateTime: this.input.SendingDateTime,
        SendingTimeOut: this.input.SendingTimeOut
      }
      arrpost.push(this.global.post(url, param))
    });

    Promise.all(arrpost).then(out => {
      console.log(out[0])
      let status = (out as any)[0].statusText
      if (status == 'Created') {
        let alert = this.alert.create({
          title: 'Sukses',
          subTitle: 'SMS berhasil dikirim',
          buttons: ['Dismiss']
        });
        alert.present();

        this.navCtrl.setRoot(OutboxPage)
      }else{
        let alert = this.alert.create({
          title: 'Gagal',
          subTitle: 'SMS gagal dikirim',
          buttons: ['Dismiss']
        });
        alert.present();
      }
    })
    // this.global.post(url, this.input)
    // .then(out => {
    //   console.log(out)
    //   let data = (out as any).data.data
    
    // })
  }
}
