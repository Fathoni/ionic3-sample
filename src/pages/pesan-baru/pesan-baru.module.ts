import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesanBaruPage } from './pesan-baru';

@NgModule({
  declarations: [
    PesanBaruPage,
  ],
  imports: [
    IonicPageModule.forChild(PesanBaruPage),
  ],
})
export class PesanBaruPageModule {}
