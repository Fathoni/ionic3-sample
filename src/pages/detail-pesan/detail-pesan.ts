import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the DetailPesanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-pesan',
  templateUrl: 'detail-pesan.html',
})
export class DetailPesanPage {
  
  details: any

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public global: GlobalProvider,
    public events: Events
  ) {
    let tujuan = ''
    if (navParams.data.item["phone.name"] != null) {
      tujuan = navParams.data.item["phone.name"]
    } else {
      tujuan = navParams.data.item.DestinationNumber
    }

    this.details = {
      id: navParams.data.item.id,
      tujuan: tujuan,
      deskripsi: navParams.data.item.TextDecoded
    }
    console.log(navParams.data.item)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPesanPage');
  }

  hapus () {
    let url = 'outbox'
    this.global.delete(url, this.details.id)
      .then(out => {
        let status = (out as any).statusText
        this.events.publish('refreshOutbox')
        this.navCtrl.pop()
        console.log(out)
      })
  }

}
