import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailPesanPage } from './detail-pesan';

@NgModule({
  declarations: [
    DetailPesanPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailPesanPage),
  ],
})
export class DetailPesanPageModule {}
