import { Injectable } from '@angular/core';
import axios from 'axios'
import { Storage } from '@ionic/storage';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  urlApi:any
  constructor(private storage: Storage) {
  }

  get (url) {
    return new Promise((resolve, reject) => {
      this.storage.get('ip_address').then((val) => {
        console.log('ip address = ',val)
        
        if (val != null) {
          this.urlApi = 'http://'+val+':3030/'
        }

        axios.get(this.urlApi+url)
        .then(function (response) {
          resolve(response)
          console.log(response);
        })
        .catch(function (error) {
          resolve(error)
          console.log(error);
        });
      });
    })
  }
  
  post (url, params) {
    return new Promise((resolve, reject) => {
      this.storage.get('ip_address').then((val) => {
        console.log('ip address = ',val)
        
        if (val != null) {
          this.urlApi = 'http://'+val+':3030/'
        }

        axios.post(this.urlApi+url, params)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          resolve(error);
        });
      });
    })
  }
  
  patch (url, params) {
    let id = params.id
    delete params.id
    console.log(params)
    return new Promise((resolve, reject) => {

      this.storage.get('ip_address').then((val) => {
        console.log('ip address = ',val)
        
        if (val != null) {
          this.urlApi = 'http://'+val+':3030/'
        }

        axios({
          method: 'patch',
          url: this.urlApi+url+'?phone='+id,
          data: params
        }).then(out => {
          resolve(out)
        }).catch(err => {
          resolve(err)
        });

      });
    })
  }

  delete (url, id) {
    return new Promise((resolve, reject) => {

      this.storage.get('ip_address').then((val) => {
        console.log('ip address = ',val)
        
        if (val != null) {
          this.urlApi = 'http://'+val+':3030/'
        }

        axios({
          method: 'delete',
          url: this.urlApi+url+'/'+id
        }).then(out => {
          resolve(out)
        }).catch(err => {
          resolve(err)
        });

      });
    })
  }

}
